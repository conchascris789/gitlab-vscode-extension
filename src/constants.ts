export const GITLAB_COM_URL = 'https://gitlab.com';
export const REVIEW_URI_SCHEME = 'gl-review';
export const CONFIG_NAMESPACE = 'gitlab';
export const CONFIG_CUSTOM_QUERIES = 'customQueries';
export const ADDED = 'added';
export const DELETED = 'deleted';
export const RENAMED = 'renamed';
export const MODIFIED = 'modified';
export const DO_NOT_SHOW_VERSION_WARNING = 'DO_NOT_SHOW_VERSION_WARNING';
export const MINIMUM_VERSION = 13.5;
